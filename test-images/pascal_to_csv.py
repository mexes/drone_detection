from xml.dom import minidom
import os

DATASET_DIR = 'train'

annotation_file = open("annotations.csv", "w") # open file to write annotation data to

data_to_save = ""

for xml_file in [f for f in os.listdir(DATASET_DIR) if f.endswith(".xml")]:
    pascal_annotation_data = minidom.parse(os.path.join(DATASET_DIR, xml_file))
    object_items = pascal_annotation_data.getElementsByTagName('object')

    path_name = pascal_annotation_data.getElementsByTagName('path')[0].firstChild.data

    for object_item in object_items:
        bbox = object_item.getElementsByTagName("bndbox")[0]
        class_name = object_item.getElementsByTagName("name")[0].firstChild.data
        xmin = bbox.getElementsByTagName("xmin")[0]
        ymin = bbox.getElementsByTagName("ymin")[0]
        xmax = bbox.getElementsByTagName("xmax")[0]
        ymax = bbox.getElementsByTagName("ymax")[0]
        # csv file: line needs to look as follows
        # path/to/image.jpg,x1,y1,x2,y2,class_name
        x1 = xmin.firstChild.data
        y1 = ymin.firstChild.data
        x2 = xmax.firstChild.data
        y2 = ymax.firstChild.data
        data_to_save += f"{path_name},{x1},{y1},{x2},{y2},{class_name}" + "\n"

annotation_file.write(data_to_save)
annotation_file.close()

    
 
# command for training model; consinder adding --steps and --epochs AND config is necessary, since anchors are so small
keras_retinanet/bin/train.py --steps 250 --epochs 6 --config /u/62/prollm2/unix/Documents/Drone_Shot_Detection/keras-retinanet/configs/drone_config.ini --weights /u/62/prollm2/unix/Documents/Drone_Shot_Detection/trained-models/resnet50_coco_best_v2.1.0.h5 csv /u/62/prollm2/unix/Documents/Drone_Shot_Detection/test-images/annotations.csv /u/62/prollm2/unix/Documents/Drone_Shot_Detection/test-images/classes.csv

# train with standard amount of epochs and with given batchsize, so reduce noise in gradient descent
keras_retinanet/bin/train.py --steps 250 --batch-size 10 --config /u/62/prollm2/unix/Documents/Drone_Shot_Detection/keras-retinanet/configs/drone_config.ini --weights /u/62/prollm2/unix/Documents/Drone_Shot_Detection/trained-models/resnet50_coco_best_v2.1.0.h5 csv /u/62/prollm2/unix/Documents/Drone_Shot_Detection/test-images/annotations.csv /u/62/prollm2/unix/Documents/Drone_Shot_Detection/test-images/classes.csv 


# for actually predicting the bounding boxes, use main.py

# that one actually shows, if the bounding boxes are corretly used
python keras_retinanet/bin/debug.py --annotations csv /u/62/prollm2/unix/Documents/Drone_Shot_Detection/test-images/annotations.csv /u/62/prollm2/unix/Documents/Drone_Shot_Detection/test-images/classes.csv

# to use actually use the trained model for detection, it needs to be converted to inference model
# BUT, if config for custom anchors was used, they also need to be set here!
keras_retinanet/bin/convert_model.py --config configs/drone_config.ini /path/to/training/model.h5 /path/to/save/inference/model.h5
